//INTRODUCTION TO NODE JS

let http = require("http");
			// modules need to be imported or required


//http modules contains method and other codes that allow us to create server and let our client communicate through HTTP



//createServer() it creates server that handles request and  responses
// .createSerer() - method has an anonymous function that handles our client and our server response.  The anonymous function int the createServer method is able to receive two objects; the request and response.
// Each request and  response parameters are object which contains the details of a request and response as well the method to handle them

//res.writeHead() - a method of the response object. This will allow us to add headers, which are additional information about our server's response. The first argument in writeHead is an HTTP satus which is used to tell the client about the status of their request

// res.end() = is a method ofa response object which end the servers respons and sends a message as a string

// .listen() - assigns a port to a server. There are several  task and process in our computer which are also designated into their specific ports

// http://localhost:4000
	//localhost - current machine
	// 4000 - port number assigned to where the process/server is listening/running from
http.createServer((req,res) => {

		//localhost:4000 is calle an endpoint
		if(req.url === "/"){
			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end(`Hi! Welcome to our homepage`)
		} else if(req.url ==="/login"){
			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('Welcome to loginpage')
		}else {
			res.writeHead(404, {'Content-Type' : 'text/plain'})
			res.end('Resource cannot be found')
		}


}).listen(4000);
console.log('Server is running on localhost : 4000')
